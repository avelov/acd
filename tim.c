#include "tim.h"
#include "gpio.h"
#include "led.h"
#include "stm32.h"

#include "print.h"
//~2 sec
#define PSC_VALUE (20 * 1024)
#define ARR_VALUE 1024

TIM_TypeDef* timer[CLICK_TYPE_COUNT] = { TIM2, TIM5 };

void handle_click(bool type)
{
    timer[type]->CR1 &= ~TIM_CR1_CEN; // turn off timer
    timer[type]->CNT = 0; // reset counter
    timer[type]->CR1 |= TIM_CR1_OPM | TIM_CR1_CEN; // restart in one pulse mode

    if(type == CLICK_TYPE_SINGLE)
        led_set_pwm(100U, 0U, 0U); // turn on red led
    if(type == CLICK_TYPE_DOUBLE)
        led_set_pwm(0U, 100U, 0U); // turn on green led
}

void TIM2_IRQHandler(void) // single click led timeout interruption
{
    uint32_t it_status = TIM2->SR & TIM2->DIER;
    if(it_status & TIM_SR_UIF)
    {
        TIM2->SR &= ~TIM_SR_UIF;
        led_set_pwm(1U, 0U, 0U); // turn off red, do not modify the others
    }
}

void TIM5_IRQHandler(void) // double click led timeout interruption
{
    uint32_t it_status = TIM5->SR & TIM5->DIER;
    if(it_status & TIM_SR_UIF)
    {
        TIM5->SR &= ~TIM_SR_UIF;
        led_set_pwm(0U, 1U, 0U); // turn off green, do not modify the others
    }
}

void init_click_timer(void)
{
    RCC->APB1ENR |= RCC_APB1ENR_TIM2EN; // clock activation
    RCC->APB1ENR |= RCC_APB1ENR_TIM5EN;
    __NOP();
    __NOP();

    for(unsigned i = 0; i < CLICK_TYPE_COUNT; i++)
    {
        // time
        timer[i]->PSC = PSC_VALUE;
        timer[i]->ARR = ARR_VALUE;

        // interrupts
        timer[i]->SR &= ~TIM_SR_UIF; // clear interrupt flag in status register
        timer[i]->DIER = TIM_DIER_UIE; // set enable bit in dma/interrupt enable register (enables interruption)

        // push psc and arr values
        timer[i]->CR1 |= TIM_CR1_URS; // update request source bit, if set update event is generated, but TIM_SR_UIF bit is not set, interruption is not sent.
        timer[i]->EGR = TIM_EGR_UG; // generates update event, PSC, ARR shadow registers are updated
        timer[i]->CR1 &= ~TIM_CR1_URS; // reenable interruptionon on update event
    }

    NVIC_EnableIRQ(TIM2_IRQn);
    NVIC_EnableIRQ(TIM5_IRQn);
}
