#ifndef ACC_DEFINES_INCLUDED
#define ACC_DEFINES_INCLUDED

#include "stm32.h"

typedef unsigned char uint8_t;

#define LIS35DE_ADDR 0x1C

#define CTRL_REG1 0x20
#define CTRL_REG3 0x22
#define CLICK_REG 0x38
#define CLICK_SRC 0x39
#define CLICK_THSY_X 0x3B
#define CLICK_THS_Z 0x3C
#define CLICK_TimeLimit 0x3D
#define CLICK_Latency 0x3E
#define CLICK_Window 0x3F

#define OUT_REG_X 0x29
#define OUT_REG_Y 0x2B
#define OUT_REG_Z 0x2D

#define VALUE_THSY_X ((15 << 4) | 15)
#define VALUE_THS_Z 8 // 0.5g increments, 0.5-7.5g
#define VALUE_Latency 60 // 1ms increments, 0-255ms
#define VALUE_TimeLimit 3 // 0.5ms increments, 0-127.5ms
#define VALUE_Window 255 // 1ms increments, 0-255ms

#define INT2_PIN_NR 8
#define EXTI_ACC_INT2_BIT EXTI_PR_PR8

#define CTRL_REG1_DR 0b10000000
#define CTRL_REG1_PD 0b01000000
#define CTRL_REG1_FS 0b00100000
#define CTRL_REG1_Zen 0b00000100
#define CTRL_REG1_Yen 0b00000010
#define CTRL_REG1_Xen 0b00000001

#define CTRL_REG3_IHL 0b10000000
#define CTRL_REG3_PP_OD 0b01000000
#define CTRL_REG3_I2CFG2 0b00100000
#define CTRL_REG3_I2CFG1 0b00010000
#define CTRL_REG3_I2CFG0 0b00001000
#define CTRL_REG3_I1CFG2 0b00000100
#define CTRL_REG3_I1CFG1 0b00000010
#define CTRL_REG3_I1CFG0 0b00000001

#define CLICK_CFG_LIR 0b01000000
#define CLICK_CFG_Double_Z 0b00100000
#define CLICK_CFG_Single_Z 0b00010000
#define CLICK_CFG_Double_Y 0b00001000
#define CLICK_CFG_Single_Y 0b00000100
#define CLICK_CFG_Double_X 0b00000010
#define CLICK_CFG_Single_X 0b00000001

#define CLICK_SRC_IA 0b01000000
#define CLICK_SRC_Double_Z 0b00100000
#define CLICK_SRC_Single_Z 0b00010000
#define CLICK_SRC_Double_Y 0b00001000
#define CLICK_SRC_Single_Y 0b00000100
#define CLICK_SRC_Double_X 0b00000010
#define CLICK_SRC_Single_X 0b00000001

#endif // ACC_DEFINES_INCLUDED
