#include "led.h"
#include "gpio.h"
#include "print.h"
#include "stm32.h"

#define RED_LED_GPIO GPIOA
#define GREEN_LED_GPIO GPIOA
#define BLUE_LED_GPIO GPIOB

#define RED_LED_PIN 6
#define GREEN_LED_PIN 7
#define BLUE_LED_PIN 0

volatile uint8_t r_level = 0;
volatile uint8_t g_level = 0;
volatile uint8_t b_level = 0;

// if parameter is 0, then it has no effect
void led_set_pwm(uint8_t r, uint8_t g, uint8_t b)
{
    r > 0 ? TIM3->CCR1 = r : (void)0;
    g > 0 ? TIM3->CCR2 = g : (void)0;
    b > 0 ? TIM3->CCR3 = b : (void)0;
    // TIM3->EGR = TIM_EGR_UG;
}

#define LED_BIT_TIME 500
#define LED_BLINK_TIME 10

void led_blink(uint8_t r, uint8_t g, uint8_t b)
{
    led_set_pwm(r, g, b);
    led_wait(LED_BLINK_TIME);
    led_set_pwm(1, 1, 1);
    led_wait(LED_BLINK_TIME / 2);
}

static void led_bit_on(void)
{
    led_set_pwm(100, 100, 100);
    led_wait(LED_BIT_TIME);
    led_set_pwm(1, 1, 1);
}

static void led_bit_off(void)
{
    led_set_pwm(1, 1, 50);
    led_wait(LED_BIT_TIME);
    led_set_pwm(1, 1, 1);
}

void led_print_pwm(uint8_t val)
{
    if (val == 0)
    {
        led_set_pwm(100, 100, 100);
        return;
    }

    led_set_pwm(100 * ((val >> 2) % 2), 100 * ((val >> 1) % 2), 100 * (val % 2));
}

void led_print(uint8_t val)
{
    for (uint8_t i = 0; i < 8; i++)
    {
        if (val % 2 == 1)
            led_bit_on();
        else
            led_bit_off();
        val >>= 1;
        led_set_pwm(1, 1, 1);
        led_wait(LED_BLINK_TIME);
    }
}

void led_wait(unsigned ms)
{
    unsigned count = ms * 250 * 16;
    while (count--)
    {
        __NOP();
        __NOP();
    }
}

void led_die(unsigned code)
{
    const unsigned blink = 200;
    for (;;)
    {
        led_set_pwm(100, 1, 1);
        led_wait(4 * blink);
        led_set_pwm(1, 1, 1);
        led_wait(2 * blink);

        for (unsigned i = 0; i < code; i++)
        {
            led_set_pwm(1, 100, 1);
            led_wait(blink);
            led_set_pwm(1, 1, 1);
            led_wait(blink);
        }
    }
}

void init_led(void)
{
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN | RCC_AHB1ENR_GPIOBEN;
    RCC->APB1ENR |= RCC_APB1ENR_TIM3EN; // clock activation
    __NOP();
    __NOP();

    RED_LED_GPIO->BSRRL = 1 << RED_LED_PIN; // turn off leds before init
    GREEN_LED_GPIO->BSRRL = 1 << GREEN_LED_PIN;
    BLUE_LED_GPIO->BSRRL = 1 << BLUE_LED_PIN;

    GPIOafConfigure(RED_LED_GPIO, RED_LED_PIN, GPIO_OType_PP, GPIO_Low_Speed, GPIO_PuPd_NOPULL, GPIO_AF_TIM3); // red
    GPIOafConfigure(GREEN_LED_GPIO, GREEN_LED_PIN, GPIO_OType_PP, GPIO_Low_Speed, GPIO_PuPd_NOPULL, GPIO_AF_TIM3); // green
    GPIOafConfigure(BLUE_LED_GPIO, BLUE_LED_PIN, GPIO_OType_PP, GPIO_Low_Speed, GPIO_PuPd_NOPULL, GPIO_AF_TIM3); // blue

    TIM3->CR1 = 0; // upcounting
    TIM3->PSC = 0; // 0-65535, also on 32-bit timers,
    TIM3->ARR = 256; // 16 bits on TIM3, TIM4; 32 bits on TIM2, TIM5

    TIM3->CCMR1 = TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC1M_1 | // 110 -> PWM Mode 1
        TIM_CCMR1_OC1PE | // Preload enable, must be.
        TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2M_1 | // 110 -> PWM Mode 1
        TIM_CCMR1_OC2PE; // Preload enable, must be.

    TIM3->CCMR2 = TIM_CCMR2_OC3M_2 | TIM_CCMR2_OC3M_1 | // 110 -> PWM Mode 1
        TIM_CCMR2_OC3PE; // Preload enable, must be.

    // capture-compare enable & polarization configuration register
    TIM3->CCER = TIM_CCER_CC1E | TIM_CCER_CC1P | TIM_CCER_CC2E | TIM_CCER_CC2P | TIM_CCER_CC3E | TIM_CCER_CC3P;

    // set initial values
    TIM3->CCR1 = r_level;
    TIM3->CCR2 = g_level;
    TIM3->CCR3 = b_level;

    TIM3->EGR = TIM_EGR_UG; // generates update event, PSC, ARR shadow registers are updated
    TIM3->CR1 |= TIM_CR1_CEN; // counter enable bit

    print("LED init done\r\n");
}
