#ifndef PRINT_INCLUDED
#define PRINT_INCLUDED

void print(const char*);
void printd(uint32_t);
void printb(uint32_t);
void printh(uint32_t);

#endif // PRINT_INCLUDED
