#include "gpio.h"
#include "stm32.h"

#include "acc-defines.h"
#include "acc.h"
#include "assert.h"
#include "i2c.h"
#include "led.h"
#include "print.h"
#include "tim.h"

// TIM2 -> single click
// TIM3 -> led pwm
// TIM4 -> I2C control watchdog (?)
// TIM5 -> double click

int main(void)
{
    init_led();
    init_click_timer();
    init_i2c();
    init_acc();
    led_set_pwm(1, 1, 1);
    for(;;)
        __WFI();
}
