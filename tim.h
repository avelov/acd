#ifndef TIM_INCLUDED
#define TIM_INCLUDED

#include <stdbool.h>

void handle_click(bool type);
void init_click_timer(void);

#define CLICK_TYPE_SINGLE 0
#define CLICK_TYPE_DOUBLE 1
#define CLICK_TYPE_COUNT 2

#endif // TIM_INCLUDED
