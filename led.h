#ifndef LED_INCLUDED
#define LED_INCLUDED

typedef unsigned char uint8_t;

void led_wait(unsigned ms);
void led_die(unsigned code);
void led_set_pwm(uint8_t r, uint8_t g, uint8_t b);
void init_led(void);
void led_print(uint8_t);
void led_print_pwm(uint8_t);
void led_blink(uint8_t, uint8_t, uint8_t);

#define LED_BLINK_RED 255, 1, 1
#define LED_BLINK_GREEN 1, 255, 1
#define LED_BLINK_BLUE 1, 1, 255

#define ERROR_LOGIC 1
#define ERROR_ACC_WTF 2
#define ERROR_I2C_BUSY_TIMEOUT 3
#define ERROR_I2C_LAZY_ERROR 4
#define ERROR_QUEUE_EMPTY 5
#define ERROR_QUEUE_FULL 6

#endif // LED_INCLUDED
