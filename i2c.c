#include <stdbool.h>
#include <stdlib.h>

#include "gpio.h"
#include "stm32.h"

#include "i2c.h"
#include "led.h"
#include "print.h"

#include "assert.h"

typedef void (*i2c_callback)(void);

#define NON_BUSY_I2C 13

#ifndef BUSY_I2C

// I2C_STATE_(IDLE|{BIT we were waiting for})
#define I2C_STATE_IDLE 0U
#define I2C_STATE_SB 1U
#define I2C_STATE_ADDR 2U

#define I2C_STATE_READ_BTF 3U
#define I2C_STATE_READ_SB 4U
#define I2C_STATE_READ_ADDR 5U
#define I2C_STATE_READ_RXNE 6U

#define I2C_STATE_WRITE_TXE 7U
#define I2C_STATE_WRITE_BTF 8U

volatile uint32_t i2c_state = 0;
volatile uint32_t i2c_counter = 0;
volatile uint32_t handler_calls = 0;
volatile uint32_t queue_size = 0;
volatile bool i2c_lock = false;

typedef union { // clang-format bug
    uint8_t value;
    uint8_t* ptr;
} RequestData;

typedef struct
{
    bool read;
    uint8_t address;
    uint8_t sub_address;
    i2c_callback callback;
    RequestData data;

} I2C_Request;

#define QUEUE_TYPE I2C_Request
#define QUEUE_BUFFER_SIZE 128U

#include "generic-queue.h"
Queue requests;

#endif

void init_i2c(void)
{
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN; // I2C1 is on GPIOB
    RCC->APB1ENR |= RCC_APB1ENR_I2C1EN;

    GPIOafConfigure(GPIOB, 8, GPIO_OType_OD, GPIO_Low_Speed, GPIO_PuPd_NOPULL, GPIO_AF_I2C1); // SCL line
    GPIOafConfigure(GPIOB, 9, GPIO_OType_OD, GPIO_Low_Speed, GPIO_PuPd_NOPULL, GPIO_AF_I2C1); // SDA line

    I2C1->CR1 = 0; // default configuration

    // clock configuration
    I2C1->CR2 = HSI_CLOCK_MHZ;
    I2C1->CCR = (HSI_CLOCK_MHZ * 1000000) / (I2C_SPEED_HZ << 1);
    I2C1->TRISE = HSI_CLOCK_MHZ + 1;

#ifndef BUSY_I2C
    I2C1->CR2 |= I2C_CR2_ITBUFEN; // buffer interruption
    I2C1->CR2 |= I2C_CR2_ITEVTEN; // event interruption
    I2C1->CR2 |= I2C_CR2_ITERREN; // error interruption

    NVIC_EnableIRQ(I2C1_EV_IRQn);
    NVIC_EnableIRQ(I2C1_ER_IRQn);
    queue_init(&requests);
#endif // not BUSY_I2C

    // enable interface
    I2C1->CR1 |= I2C_CR1_PE;

    print("I2C init done\r\n");
}

void i2c_read(uint8_t address, uint8_t sub_address, uint8_t* out) { i2c_read_callback(address, sub_address, out, NULL); }

void i2c_write(uint8_t address, uint8_t sub_address, uint8_t value) { i2c_write_callback(address, sub_address, value, NULL); }

#ifdef BUSY_I2C

#define MAX_TRIES 8 * 1000 * 1000
#define WAIT_FOR(BIT_FLAG)                                                                                                                                     \
    {                                                                                                                                                          \
        unsigned i = 0;                                                                                                                                        \
        while((I2C1->SR1 & BIT_FLAG) == 0 && i < MAX_TRIES)                                                                                                    \
            i++;                                                                                                                                               \
        if(i == MAX_TRIES)                                                                                                                                     \
            led_die(ERROR_I2C_BUSY_TIMEOUT);                                                                                                                   \
    }

void i2c_read_callback(uint8_t address, uint8_t sub_address, uint8_t* out, i2c_callback callback)
{
    I2C1->CR1 |= I2C_CR1_START; // send start signal, MT mode
    WAIT_FOR(I2C_SR1_SB); // wait until "start" bit is set

    I2C1->DR = address << 1; // send address & write mode to send sub_addres
    WAIT_FOR(I2C_SR1_ADDR); // wait for ADDR bit (address ACK from slave)

    I2C1->SR2; // clears ADDR bit in SR1, recommended by the reference, page 491
    I2C1->DR = sub_address; // send sub_address to the device
    WAIT_FOR(I2C_SR1_BTF); // wait for "byte transfer finished" bit

    I2C1->CR1 |= I2C_CR1_START; // send repeated start to enter MR mode
    WAIT_FOR(I2C_SR1_SB); // wait for start bit

    I2C1->DR = (address << 1) | 1U; // send slave address to bus & receive mode bit
    WAIT_FOR(I2C_SR1_ADDR); // wait unitl address reception is confirmed
    I2C1->CR1 &= ~I2C_CR1_ACK; // set received byte as last (this refers to the
    // future incoming data byte)
    I2C1->SR2; // clear the ADDR bit
    I2C1->CR1 |= I2C_CR1_STOP; // send stop request

    WAIT_FOR(I2C_SR1_RXNE); // wait for receiver non empty (DR can be read then)
    *out = I2C1->DR;

    if(callback != NULL)
        callback();
}

void i2c_write_callback(uint8_t address, uint8_t sub_address, uint8_t value, i2c_callback callback)
{

    I2C1->CR1 |= I2C_CR1_START; // send start signal, MT mode
    WAIT_FOR(I2C_SR1_SB); // wait until "start" bit is not set

    I2C1->DR = address << 1; // send address & write mode
    WAIT_FOR(I2C_SR1_ADDR); // wait for ADDR bit (address ACK from slave)

    I2C1->SR2; // clears ADDR bit in SR1, recommended by the reference, page 491
    I2C1->DR = sub_address; // send sub_address to the device
    WAIT_FOR(I2C_SR1_TXE); // wait until transmission queue is not empty

    I2C1->DR = value;
    WAIT_FOR(I2C_SR1_BTF); // wait for byte transfer finish

    I2C1->CR1 |= I2C_CR1_STOP; // send stop signal

    if(callback != NULL) // always false if busy i2c
        callback();
}

#endif // BUSY_I2C

#ifndef BUSY_I2C

void i2c_start_next_request(void)
{
    I2C1->CR1 |= I2C_CR1_START;
    i2c_state = I2C_STATE_SB;
}
void I2C1_EV_IRQHandler(void)
{
    volatile uint32_t sr = I2C1->SR1;
    i2c_callback callback = NULL;
    handler_calls++;

    i2c_lock = true;
    switch(i2c_state)
    {
    default:
        break;

    case I2C_STATE_SB: // START bit received
        if(!(sr & I2C_SR1_SB))
            break;
        I2C1->DR = queue_peek(&requests, 0)->address << 1;
        i2c_state = I2C_STATE_ADDR;
        break;
    case I2C_STATE_ADDR: // ADDR bit received
        if(!(sr & I2C_SR1_ADDR))
            break;
        I2C1->SR2;
        I2C1->DR = queue_peek(&requests, 0)->sub_address;
        i2c_state = queue_peek(&requests, 0)->read ? I2C_STATE_READ_BTF : I2C_STATE_WRITE_TXE;
        break;

    case I2C_STATE_WRITE_TXE: // TXE bit received while writting
        if(!(sr & I2C_SR1_TXE))
            break;
        I2C1->DR = queue_peek(&requests, 0)->data.value;
        i2c_state = I2C_STATE_WRITE_BTF;
        break;
    case I2C_STATE_WRITE_BTF:; // BTF bit received while writting
        if(!(sr & I2C_SR1_BTF))
            break;
        callback = queue_peek(&requests, 0)->callback;
        queue_pop(&requests);
        queue_size--;
        I2C1->CR1 |= I2C_CR1_STOP;
        i2c_state = I2C_STATE_IDLE;
        if(!queue_empty(&requests))
            i2c_start_next_request();
        break;

    case I2C_STATE_READ_BTF: // BTF bit received while writting
        if(!(sr & I2C_SR1_BTF))
            break;
        I2C1->CR1 |= I2C_CR1_START; // repeated start to enter MR mode
        i2c_state = I2C_STATE_READ_SB;

        break;
    case I2C_STATE_READ_SB: // START bit received while reading (repeated start to enter MR mode)
        if(!(sr & I2C_SR1_SB))
            break;
        I2C1->DR = (queue_peek(&requests, 0)->address << 1) | 1U;
        i2c_state = I2C_STATE_READ_ADDR;
        break;
    case I2C_STATE_READ_ADDR: // ADDR bit received while reading (in MR mode)
        if(!(sr & I2C_SR1_ADDR))
            break;
        I2C1->CR1 &= ~I2C_CR1_ACK;
        I2C1->SR2;
        I2C1->CR1 |= I2C_CR1_STOP;
        i2c_state = I2C_STATE_READ_RXNE;
        break;
    case I2C_STATE_READ_RXNE: // RXNE bit received while reading (in MR mode)
        if(!(sr & I2C_SR1_RXNE))
            break;
        *(queue_peek(&requests, 0)->data.ptr) = I2C1->DR;
        callback = queue_peek(&requests, 0)->callback;
        queue_pop(&requests);
        queue_size--;
        i2c_state = I2C_STATE_IDLE;
        if(!queue_empty(&requests))
            i2c_start_next_request();
        break;
    }
    i2c_lock = false;
    if(callback != NULL) // moved out of the critical section, otherwise callback sending request would result in deadlock
        callback();
}

void I2C1_ER_IRQHandler(void) { led_die(ERROR_I2C_LAZY_ERROR); }

void i2c_add_request(uint8_t address, uint8_t sub_address, RequestData data, bool read, i2c_callback callback)
{
    I2C_Request req;
    req.read = read;
    req.address = address;
    req.sub_address = sub_address;
    req.callback = callback;
    req.data = data;
    i2c_counter++;

    // when adding to queue, interrupts must be disabled
    // because of operations on queue in I2C interruption handler

    __disable_irq();
    while(i2c_lock) // wait until I2C Interruption is done
        led_blink(LED_BLINK_BLUE);
    queue_push(&requests, req);
    queue_size++;
    if(i2c_state == I2C_STATE_IDLE)
        i2c_start_next_request();
    __enable_irq();
}

void i2c_read_callback(uint8_t address, uint8_t sub_address, uint8_t* out, i2c_callback callback)
{
    RequestData data;
    data.ptr = out;
    i2c_add_request(address, sub_address, data, true, callback);
}

void i2c_write_callback(uint8_t address, uint8_t sub_address, uint8_t value, i2c_callback callback)
{
    RequestData data;
    data.value = value;
    i2c_add_request(address, sub_address, data, false, callback);
}

#endif // not BUSY_I2C
