#ifndef I2C_INCLUDED
#define I2C_INCLUDED

typedef unsigned char uint8_t;

#define I2C_SPEED_HZ 100000
#define HSI_CLOCK_MHZ 16

void init_i2c(void);

void i2c_read(uint8_t address, uint8_t sub_address, uint8_t* out);
void i2c_write(uint8_t address, uint8_t sub_address, uint8_t value);

void i2c_read_callback(uint8_t address, uint8_t sub_address, uint8_t* out, void (*callback)(void));
void i2c_write_callback(uint8_t address, uint8_t sub_address, uint8_t value, void (*callback)(void));

#endif // I2C_INCLUDED
