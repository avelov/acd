#include <stdbool.h>

#include "gpio.h"
#include "stm32.h"

#include "acc-defines.h"
#include "acc.h"
#include "assert.h"
#include "i2c.h"
#include "led.h"
#include "print.h"
#include "tim.h"

void acc_read_config(void);
void acc_enable_irq(void);

uint8_t click_src; // value of acc register

void init_acc(void)
{
    i2c_write(LIS35DE_ADDR, CTRL_REG1, 0U); // reset accelerometer configuration
    i2c_write(LIS35DE_ADDR, CTRL_REG3, 0U);

    RCC->APB1ENR |= RCC_AHB1ENR_GPIOAEN; // acc interruption on pin 8 GPIOA
    RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN; // EXTI

    // setup accelerometer configuration
    uint8_t cfg1 = CTRL_REG1_PD; // active mode
    uint8_t cfg3 = CTRL_REG3_I2CFG2 | CTRL_REG3_I2CFG1 | CTRL_REG3_I2CFG0; // single and double click on INT2
    uint8_t click_cfg = CLICK_CFG_LIR | CLICK_CFG_Single_X | CLICK_CFG_Double_X; // click detection on axis Z (up/down)

    // write config to accelerometer
    i2c_write(LIS35DE_ADDR, CTRL_REG1, cfg1);
    i2c_write(LIS35DE_ADDR, CTRL_REG3, cfg3);
    i2c_write(LIS35DE_ADDR, CLICK_REG, click_cfg);
    i2c_write(LIS35DE_ADDR, CLICK_THS_Z, VALUE_THS_Z);
    i2c_write(LIS35DE_ADDR, CLICK_THSY_X, VALUE_THSY_X);
    i2c_write(LIS35DE_ADDR, CLICK_Latency, VALUE_Latency);
    i2c_write(LIS35DE_ADDR, CLICK_TimeLimit, VALUE_TimeLimit);
    i2c_write_callback(LIS35DE_ADDR, CLICK_Window, VALUE_Window, acc_enable_irq); // when all requests are done - turn on interruptions

    acc_read_config();
}

void acc_enable_irq(void)
{
    // INT2 config
    GPIOinConfigure(GPIOA, INT2_PIN_NR, GPIO_PuPd_DOWN, EXTI_Mode_Interrupt, EXTI_Trigger_Rising);
    NVIC_EnableIRQ(EXTI9_5_IRQn);
    print("ACC init done\r\n");
}

// callback when data requested in IRQHandler is ready
void click_src_received(void)
{
    assert(click_src & CLICK_SRC_IA);

    if(click_src & CLICK_SRC_IA)
    {
        if(click_src & CLICK_SRC_Single_X)
            handle_click(CLICK_TYPE_SINGLE);
        if(click_src & CLICK_SRC_Double_X)
            handle_click(CLICK_TYPE_DOUBLE);
    }
    if(GPIOA->IDR & (1 << INT2_PIN_NR)) // PIN must be low to avoid deadlock
        i2c_read_callback(LIS35DE_ADDR, CLICK_SRC, &click_src, click_src_received);
}

// INT 2 from acc (single/double click)
void EXTI9_5_IRQHandler(void)
{
    assert(EXTI->PR & EXTI_ACC_INT2_BIT);
    if(EXTI->PR & EXTI_ACC_INT2_BIT)
    {
        i2c_read_callback(LIS35DE_ADDR, CLICK_SRC, &click_src, click_src_received);
        EXTI->PR = EXTI_ACC_INT2_BIT;
    }
}

// DEBUG

void acc_read_config(void)
{
    uint8_t ths = 255, latency = 255, timelimit = 255, window = 255;
    uint8_t ctrl1 = 255, ctrl3 = 255, click = 255;

    i2c_read(LIS35DE_ADDR, CTRL_REG1, &ctrl1);
    i2c_read(LIS35DE_ADDR, CTRL_REG3, &ctrl3);
    i2c_read(LIS35DE_ADDR, CLICK_REG, &click);
    i2c_read(LIS35DE_ADDR, CLICK_THS_Z, &ths);
    i2c_read(LIS35DE_ADDR, CLICK_Latency, &latency);
    i2c_read(LIS35DE_ADDR, CLICK_TimeLimit, &timelimit);
    i2c_read(LIS35DE_ADDR, CLICK_Window, &window);

    led_set_pwm(1, 100, 1);
    led_wait(2000);
    led_set_pwm(1, 1, 1);

    print("ACC config:");
    printb(ths);
    printb(latency);
    printb(timelimit);
    printb(window);
    printb(ctrl1);
    printb(ctrl3);
    printb(click);
    print("\r\n");
}
