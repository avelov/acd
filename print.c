#include <stdbool.h>

#include "gpio.h"
#include "stm32.h"

#define USART_Mode_Tx USART_CR1_TE
#define USART_Enable USART_CR1_UE
#define USART_WordLength_8b 0x0000
#define USART_Parity_No 0x0000
#define USART_StopBits_1 0x0000
#define USART_FlowControl_None 0x0000
#define HSI_HZ 16000000U

static void usart_init(void)
{
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
    RCC->APB1ENR |= RCC_APB1ENR_USART2EN;
    __NOP();
    __NOP();

    GPIOafConfigure(GPIOA, 2, GPIO_OType_PP, GPIO_Fast_Speed, GPIO_PuPd_NOPULL, GPIO_AF_USART2);
    GPIOafConfigure(GPIOA, 3, GPIO_OType_PP, GPIO_Fast_Speed, GPIO_PuPd_UP, GPIO_AF_USART2);

    USART2->CR1 = USART_Mode_Tx | USART_WordLength_8b | USART_Parity_No;
    USART2->CR2 = USART_StopBits_1;
    USART2->CR3 = USART_FlowControl_None;

    uint32_t const baudrate = 9600U;
    USART2->BRR = (HSI_HZ + (baudrate / 2U)) / baudrate;

    USART2->CR1 |= USART_Enable;
}

static int usart_can_write(void) { return USART2->SR & USART_SR_TXE; }

static void usart_write(char c) { USART2->DR = c; }

static bool ready = false;

void print(const char* str)
{
    if(!ready)
        usart_init();

    for(uint16_t i = 0; str[i] != 0; i++)
    {
        while(!usart_can_write())
            ;
        usart_write(str[i]);
    }
}

static void printn(uint32_t val, uint8_t base)
{
    const int BUFFER_SIZE = 42;
    char buffer[BUFFER_SIZE];

    // init
    for(uint8_t i = 0; i < BUFFER_SIZE; i++)
        buffer[i] = 0;

    // buffer[BUFFER_SIZE -1] == "\0" at this point
    // conversion below
    if(val == 0)
        buffer[BUFFER_SIZE - 2] = '0';
    else
    {
        for(uint8_t i = BUFFER_SIZE - 2; val > 0; i--)
        {
            buffer[i] = val % base >= 10 ? 'A' + (val % base) - 10 : '0' + val % base;
            val /= base;
        }
    }

    // search for the start of the number
    uint8_t i = 0;
    while(buffer[i] == 0)
        i++;
    // add prefix
    if(base == 2)
    {
        buffer[--i] = 'b';
        buffer[--i] = '0';
    }
    if(base == 16)
    {
        buffer[--i] = 'x';
        buffer[--i] = '0';
    }
    buffer[--i] = ' ';
    print(buffer + i);
}

void printd(uint32_t val) { printn(val, 10); }

void printb(uint32_t val) { printn(val, 2); }

void printh(uint32_t val) { printn(val, 16); }
