#ifndef GENERIC_QUEUE_HEADER
#define GENERIC_QUEUE_HEADER

#include "led.h"
#include <string.h>

typedef struct
{
    QUEUE_TYPE buffer[QUEUE_BUFFER_SIZE];
    unsigned read;
    unsigned write;
} Queue;

static void queue_init(Queue* q)
{
    q->read = 0;
    q->write = 0;
}
static int queue_empty(Queue* q) { return q->read == q->write; }

static int queue_full(Queue* q) { return ((q->write + 1) % QUEUE_BUFFER_SIZE) == q->read; }

static void queue_pop(Queue* q)
{
    if(queue_empty(q))
        led_die(ERROR_QUEUE_EMPTY);

    q->read = (q->read + 1) % QUEUE_BUFFER_SIZE;
}

static void queue_push(Queue* q, QUEUE_TYPE val)
{
    if(queue_full(q))
        led_die(ERROR_QUEUE_FULL);
    q->buffer[q->write] = val;
    q->write = (q->write + 1) % QUEUE_BUFFER_SIZE;
}

static QUEUE_TYPE* queue_peek(Queue* q, int skip) { return &(q->buffer[(q->read + skip) % QUEUE_BUFFER_SIZE]); }

#endif // GENERIC_QUEUE_HEADER
