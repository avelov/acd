#ifndef ASSERT_INCLUDED
#define ASSERT_INCLUDED

#include "print.h"

#define assert(x)                                                                                                                                              \
    {                                                                                                                                                          \
        if(!(x))                                                                                                                                               \
        {                                                                                                                                                      \
            print("Assertion failed (");                                                                                                                       \
            print(#x);                                                                                                                                         \
            print(") in file ");                                                                                                                               \
            print(__FILE__);                                                                                                                                   \
            print(" on line");                                                                                                                                 \
            printd((uint32_t)(__LINE__));                                                                                                                      \
            print("\r\n");                                                                                                                                     \
        }                                                                                                                                                      \
    }

#endif // ASSERT_INCLUDED
